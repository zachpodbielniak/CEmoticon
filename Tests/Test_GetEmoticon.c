#include <PodNet/CUnitTest/CUnitTest.h>
#include "../Src/Emoticon.h"


TEST_CALLBACK(__GetEmoticon)
{
	TEST_BEGIN("GetEmoticons");

	LPCSTR lpcszTest;

	lpcszTest = GetEmoticon(EMOTICON_SMILE);
	TEST_EXPECT_STRING_EQUAL(lpcszTest, ":)");


	lpcszTest = GetEmoticon(EMOTICON_FACE_HAPPY);
	TEST_EXPECT_STRING_EQUAL(lpcszTest, "( ͡° ͜ʖ ͡°)");

	TEST_END();
}


TEST_CALLBACK(__GetEmoticonWide)
{
	TEST_BEGIN("GetEmoticonsWide");

	LPCWSTR lpcwszTest;

	lpcwszTest = GetEmoticonWide(EMOTICON_SMILE);
	TEST_EXPECT_VALUE_EQUAL(0, wcscmp(lpcwszTest, L":)"));
	
	lpcwszTest = GetEmoticonWide(EMOTICON_FACE_HAPPY);
	TEST_EXPECT_VALUE_EQUAL(0, wcscmp(lpcwszTest, L"( ͡° ͜ʖ ͡°)"));

	TEST_END();
}


TEST_START("CEmoticon");
TEST_ADD(__GetEmoticon);
TEST_ADD(__GetEmoticonWide);
TEST_RUN();
TEST_CONCLUDE();