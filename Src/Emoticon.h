/*

  ____ _____                 _   _                 
 / ___| ____|_ __ ___   ___ | |_(_) ___ ___  _ __  
| |   |  _| | '_ ` _ \ / _ \| __| |/ __/ _ \| '_ \ 
| |___| |___| | | | | | (_) | |_| | (_| (_) | | | |
 \____|_____|_| |_| |_|\___/ \__|_|\___\___/|_| |_|

C Library For Emoticons.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#ifndef CEMOTICON_H
#define CEMOTICON_H


#include "Defs.h"


typedef enum __EMOTICON
{
	/* Basic */
	EMOTICON_SMILE 		= 0,

	/* Faces */
	EMOTICON_FACE_SHRUG,
	EMOTICON_FACE_HAPPY,
	EMOTICON_FACE_TOAST,
	EMOTICON_FACE_SHADES_SAD,
	EMOTICON_FACE_SHADES_GUNS,
	EMOTICON_FACE_WANNA_FIGHT,
	EMOTICON_FACE_MUSTACHE,
	EMOTICON_FACE_BIG_NOSE_SMIRK,
	
	/* Money */
	EMOTICON_MONEY_BILL_5,
	EMOTICON_MONEY_BILL_DONGER,

	/* Table */
	EMOTICON_TABLE_RESET,


	__EMOTICON_TABLE_SIZE
} EMOTICON;




/*
	OnSuccess: Returns a pointer to an internal
	string representing the requested emoticon.

	OnFailure: Return a NULLPTR. Typically this
	is occurred by an out of bounds on emoticon
	table size exception.
*/
_Success_(return != NULLPTR, _Non_Locking_)
EMOTICON_API
LPCSTR
GetEmoticon(
	_In_		EMOTICON	emValue
);




/*
	OnSuccess: Returns a pointer to an internal
	wide string representing the requested
	emoticon.

	OnFailure: Return a NULLPTR. Typically this
	is occurred by an out of bounds on emoticon
	table size exception.
*/
_Success_(return != NULLPTR, _Non_Locking_)
EMOTICON_API
LPCWSTR
GetEmoticonWide(
	_In_		EMOTICON	emValue
);



#endif