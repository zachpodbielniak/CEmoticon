/*

  ____ _____                 _   _                 
 / ___| ____|_ __ ___   ___ | |_(_) ___ ___  _ __  
| |   |  _| | '_ ` _ \ / _ \| __| |/ __/ _ \| '_ \ 
| |___| |___| | | | | | (_) | |_| | (_| (_) | | | |
 \____|_____|_| |_| |_|\___/ \__|_|\___\___/|_| |_|

C Library For Emoticons.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#ifndef CEMOTICON_DEFS_H
#define CEMOTICON_DEFS_H


#include <PodNet/PodNet.h>
#include <PodNet/CString/CString.h>
#include <PodNet/CString/CHeapString.h>


#define EMOTICON_API			EXTERN

#endif