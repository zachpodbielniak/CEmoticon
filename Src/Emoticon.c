/*

  ____ _____                 _   _                 
 / ___| ____|_ __ ___   ___ | |_(_) ___ ___  _ __  
| |   |  _| | '_ ` _ \ / _ \| __| |/ __/ _ \| '_ \ 
| |___| |___| | | | | | (_) | |_| | (_| (_) | | | |
 \____|_____|_| |_| |_|\___/ \__|_|\___\___/|_| |_|

C Library For Emoticons.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "Emoticon.h"


/* Tables */
LPSTR dlpszTable[__EMOTICON_TABLE_SIZE] = { 0 };
LPWSTR dlpwszTable[__EMOTICON_TABLE_SIZE] = { 0 };



/* Runs On Opening From dlopen() */
__attribute__((constructor))
INTERNAL_OPERATION
VOID 
__SharedObjectMain(
	VOID
){
	/* Basic */
	dlpszTable[EMOTICON_SMILE] = ":)";
	dlpwszTable[EMOTICON_SMILE] = L":)";

	/* Faces */
	dlpszTable[EMOTICON_FACE_SHRUG] = "¯\\_(ツ)_/¯";
	dlpwszTable[EMOTICON_FACE_SHRUG] = L"¯\\_(ツ)_/¯";

	dlpszTable[EMOTICON_FACE_HAPPY] = "( ͡° ͜ʖ ͡°)";
	dlpwszTable[EMOTICON_FACE_HAPPY] = L"( ͡° ͜ʖ ͡°)";

	dlpszTable[EMOTICON_FACE_TOAST] = "ʕ•ᴥ•ʔ";
	dlpwszTable[EMOTICON_FACE_TOAST] = L"ʕ•ᴥ•ʔ";

	dlpszTable[EMOTICON_FACE_SHADES_SAD] = "(▀̿Ĺ̯▀̿ ̿)";
	dlpwszTable[EMOTICON_FACE_SHADES_SAD] = L"(▀̿Ĺ̯▀̿ ̿)";

	dlpszTable[EMOTICON_FACE_SHADES_GUNS] = "̿̿ ̿̿ ̿̿ ̿'̿'\\̵͇̿̿\\з= ( ▀ ͜͞ʖ▀) =ε/̵͇̿̿/’̿’̿ ̿ ̿̿ ̿̿ ̿̿";
	dlpwszTable[EMOTICON_FACE_SHADES_GUNS] = L"̿̿ ̿̿ ̿̿ ̿'̿'\\̵͇̿̿\\з= ( ▀ ͜͞ʖ▀) =ε/̵͇̿̿/’̿’̿ ̿ ̿̿ ̿̿ ̿̿";

	dlpszTable[EMOTICON_FACE_WANNA_FIGHT] = "(ง'̀-'́)ง";
	dlpwszTable[EMOTICON_FACE_WANNA_FIGHT] = L"(ง'̀-'́)ง"; 

	dlpszTable[EMOTICON_FACE_MUSTACHE] = "( ͡°╭͜ʖ╮͡° )";
	dlpwszTable[EMOTICON_FACE_MUSTACHE] = L"( ͡°╭͜ʖ╮͡° )";

	dlpszTable[EMOTICON_FACE_BIG_NOSE_SMIRK] = "(͡ ͡° ͜ つ ͡͡°)";
	dlpwszTable[EMOTICON_FACE_BIG_NOSE_SMIRK] = L"(͡ ͡° ͜ つ ͡͡°)";

	/* Money */
	dlpszTable[EMOTICON_MONEY_BILL_5] = "[̲̅$̲̅(̲̅5̲̅)̲̅$̲̅]";
	dlpwszTable[EMOTICON_MONEY_BILL_5] = L"[̲̅$̲̅(̲̅5̲̅)̲̅$̲̅]";

	dlpszTable[EMOTICON_MONEY_BILL_DONGER] = "[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]";
	dlpwszTable[EMOTICON_MONEY_BILL_DONGER] = L"[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]";


	/* Table */
	dlpszTable[EMOTICON_TABLE_RESET] = "┬─┬ノ( º _ ºノ)";
	dlpwszTable[EMOTICON_TABLE_RESET] = L"┬─┬ノ( º _ ºノ)";
}




_Success_(return != NULLPTR, _Non_Locking_)
EMOTICON_API
LPCSTR
GetEmoticon(
	_In_		EMOTICON	emValue
){
	if (__EMOTICON_TABLE_SIZE <= emValue)
	{ return NULLPTR; }
	
	return (LPCSTR)dlpszTable[emValue];
}




_Success_(return != NULLPTR, _Non_Locking_)
EMOTICON_API
LPCWSTR
GetEmoticonWide(
	_In_		EMOTICON	emValue
){
	if (__EMOTICON_TABLE_SIZE <= emValue)
	{ return NULLPTR; }

	return (LPCWSTR)dlpwszTable[emValue];
}