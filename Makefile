#  ____           _ __  __  ___  
# |  _ \ ___   __| |  \/  |/ _ \
# | |_) / _ \ / _` | |\/| | | | |
# |  __/ (_) | (_| | |  | | |_| |
# |_|   \___/ \__,_|_|  |_|\__\_\

# C Library For Emoticons.
# Copyright (C) 2019 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


CC = gcc
ASM = nasm
STD = -std=c89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type -Wno-stringop-truncation #-Wno-switch-enum
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__

OPTIMIZE = -O2 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
#MARCH = -march=native
#MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
#CC_FLAGS += $(MARCH)
#CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable

ASM_FLAGS = -Wall


FILES = Emoticon.o
FILES_D = Emoticon_d.o


TEST = MessageQueue_Test QueueServer_Test QueueClient_Test




all:	bin libemoticon.so 
debug:	bin libemoticon_d.so 
test: 	bin $(TEST)

bin:
	mkdir -p bin/

clean: 	bin
	rm -rf bin/

check:	bin 
	cppcheck . --std=c89 -j $(shell nproc) --inline-suppr --error-exitcode=1



# Install

install:
	cp bin/libemoticon.so /usr/lib/

install_debug:
	cp bin/libemoticon_d.so /usr/lib/

install_headers:
	rm -rf /usr/include/Emoticon/
	mkdir -p /usr/include/Emoticon
	find . -type f -name "*.h" -exec install -D {} /usr/include/Emoticon/{} \;
	mv /usr/include/PodMQ/Src/* /usr/include/Emoticon
	rm -rf /usr/include/Emoticon/Src


# Libraries

libemoticon.so:	$(FILES)
	$(CC) -shared -fPIC -s -o bin/libemoticon.so bin/*.o -pthread -lpodnet $(STD) $(OPTIMIZE) $(MARCH)
	rm bin/*.o 

libemoticon_d.so:	$(FILES_D)
	$(CC) -g -shared -fPIC -o bin/libemoticon_d.so bin/*_d.o -pthread -lpodnet_d $(STD)
	rm bin/*.o 



Emoticon.o:
	$(CC) -fPIC -c -o bin/Emoticon.o Src/Emoticon.c $(CC_FLAGS)

Emoticon_d.o:
	$(CC) -g -fPIC -c -o bin/Emoticon_d.o Src/Emoticon.c $(CC_FLAGS_D)




GetEmoticon_Test:
	$(CC) -g -fPIC -o bin/GetEmoticon_Test Tests/Test_GetEmoticon.c -lpodnet_d -lemoticon_d $(CC_FLAGS_T)

