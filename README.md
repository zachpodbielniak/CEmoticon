# CEmoticon

C Library For Emoticons.

## License

![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)

```
  ____ _____                 _   _                 
 / ___| ____|_ __ ___   ___ | |_(_) ___ ___  _ __  
| |   |  _| | '_ ` _ \ / _ \| __| |/ __/ _ \| '_ \ 
| |___| |___| | | | | | (_) | |_| | (_| (_) | | | |
 \____|_____|_| |_| |_|\___/ \__|_|\___\___/|_| |_|

C Library For Emoticons.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

CEmoticon is licensed under the AGPLv3 license. Don't like it? Go else where.

## Donate
Like CEmoticon? You use it yourself, or for your infrastructure? Why not donate to help make it better! I really appreciate any and all donations.

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59
